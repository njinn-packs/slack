from slackclient import SlackClient


class SendMessage():

    authentication = None
    message = None
    channel_id = None

    def run(self):
        """
        Sends a message to a channel
        """

        if not self.authentication or not self.authentication.get('token'):
            raise Exception("No token for authentication provided")

        if not self.channel_id:
            raise Exception("Channel is required")

        sc = SlackClient(self.authentication.get('token'))

        print(f"Sending message to channel {self.channel_id}")

        res = sc.api_call(
            "chat.postMessage",
            channel=self.channel_id,
            text=self.message,
            as_user=True
        )

        if not res.get('ok'):
            raise Exception(res)

        return {'response': res}
