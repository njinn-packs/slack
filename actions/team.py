from slackclient import SlackClient


class InviteUser():

    authentication = None
    email = None
    channels = None

    def run(self):
        """
            Calls the users.admin.invite API from slack using the official
            SlackClient and a user token.
        """

        if not self.authentication or not self.authentication.get('token'):
            raise Exception("No token for authentication provided")

        if not self.email:
            raise Exception("Require an email to send invitation")

        sc = SlackClient(self.authentication.get('token'))

        print(f"Inviting {self.email} to Slack.")

        result = sc.api_call(
            "users.admin.invite",
            email=self.email,
            channels=self.channels
        )

        if not result.get('ok'):
            raise Exception(result.get('error'))
        else:
            print("Invitation sent.")

        return result
